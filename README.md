# README #

The purpose of the project is to build a sleek, fast, full-featured picture gallery application with multilingual support using available jQuery plugin and the latest .NET technologies.

The following technologies are used: - Asp.Net MVC 3 with single project area - Galleriffic 2.1 jQuery plugin for rendering rich, fast-performing photo galleries - XSLT transformations and XML

The following features and techniques are used: - Fully localised application using XML files for storage of culture specific wordings. Ability to add unlimited number of localised versions, by simply creating a new culture folder with two XML files for translations of UI controls and images’ descriptions. - Custom ViewEngine? that renders XML using XSLT. It writes view templates as XSLT transformations and use them to render XML documents. - Support for default WebFormViewEngine? functionality which allows to render mixture of WebForm? views with XSLT views