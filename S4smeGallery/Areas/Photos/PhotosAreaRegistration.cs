﻿using System.Web.Mvc;

namespace S4smeGallery.Areas.Photos
{
    public class PhotosAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Photos";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "photos_default",
                "{culture}/photos/{controller}/{action}/{id}",
                new { culture = "en-IE", controller = "Tiles", action = "Index", id = "" },
                new { culture = "^[a-zA-Z]{2,2}-[a-zA-Z]{2,2}$" }
            );
        }
    }
}
