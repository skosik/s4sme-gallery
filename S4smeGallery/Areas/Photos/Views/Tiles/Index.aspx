<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Photos/Views/Shared/Photos.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="_titlePlaceHolder" runat="server" >
  <% Html.RenderPartial("_title"); %>  
</asp:Content>

 <asp:Content ID="Content2" ContentPlaceHolderID="_contentPlaceHolder" runat="server">
   <% Html.RenderPartial("_content"); %>
 </asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="_footerPlaceHolder" runat="server">
	<% Html.RenderPartial("_footer"); %>
</asp:Content>
