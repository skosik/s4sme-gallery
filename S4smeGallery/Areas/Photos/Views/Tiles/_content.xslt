﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:output method="html" indent="yes"/>
  <xsl:template match="/">
    <div id="content">
      <ul id="albums">
        <xsl:for-each select="root/albums/album">
          <li>
            <xsl:choose>
              <xsl:when test="./@selected = 'true'">
                <strong><xsl:value-of select="./@title"/></strong>
              </xsl:when>
              <xsl:otherwise>
                <a href="photos?albumId={./@id}"><xsl:value-of select="./@title"/></a>
              </xsl:otherwise>
            </xsl:choose>          
          </li>
        </xsl:for-each>
      </ul>
      <hr/>
      <div id="gallery" class="content">
        <div id="controls" class="controls"></div>
        <div class="slideshow-container">
          <div id="loading" class="loader"></div>
          <div id="slideshow" class="slideshow"></div>
        </div>
        <div id="caption" class="caption-container"></div>
      </div>
      <div id="thumbs" class="navigation">
        <ul class="thumbs noscript">
          <xsl:for-each select="root/images/image">
            <li>
              <a class="thumb" href="../Areas/Photos/Content/albums/{../@albumId}/{./@name}" title="{./@title}">
                <img src="../Areas/Photos/Content/albums/{../@albumId}/thumbs/{./@name}" alt="{./@title}" />
              </a>
              <div class="caption">
                <div class="image-title">
                  <xsl:value-of select="./@title"/>
                </div>
                <div class="image-desc">
                  <xsl:value-of select="./@desc"/>
                </div>
              </div>
            </li>
          </xsl:for-each>
        </ul>
      </div>
      <div style="clear: both;">
      </div>
      <div id="endContent"></div>
    </div>
  </xsl:template>
</xsl:stylesheet>
