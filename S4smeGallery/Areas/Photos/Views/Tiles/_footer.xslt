﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/">
      <div id="footer">
      <p>
        <strong><xsl:value-of select="root/label[@id='footerLabel']/@text"/></strong><br />
        <em>2009 www.s4sme.com</em>
      </p>
      </div>
      <script type="text/javascript">
        jQuery(document).ready(function($) {

        // We only want these styles applied when javascript is enabled
        $('div.navigation').css({ 'width': '300px', 'float': 'left' });
        $('div.content').css('display', 'block');

        // Initially set opacity on thumbs and add
        // additional styling for hover effect on thumbs
        var onMouseOutOpacity = 0.67;
        $('#thumbs ul.thumbs li').opacityrollover({
        mouseOutOpacity:   onMouseOutOpacity,
        mouseOverOpacity:  1.0,
        fadeSpeed:         'fast',
        exemptionSelector: '.selected'
        });

        // Initialize Advanced Galleriffic Gallery
        var gallery = $('#thumbs').galleriffic({

        delay: 5000,
        numThumbs: 12,
        preloadAhead: 10,
        enableTopPager: true,
        enableBottomPager: true,
        maxPagesToShow: 7,
        imageContainerSel: '#slideshow',
        controlsContainerSel: '#controls',
        captionContainerSel: '#caption',
        loadingContainerSel: '#loading',
        renderSSControls: true,
        renderNavControls: true,
        playLinkText: '<xsl:value-of select="root/label[@id='jsPlayLinkText']/@text"/>',
             pauseLinkText: '<xsl:value-of select="root/label[@id='jsPauseLinkText']/@text"/>',
             prevLinkText: '&#8249; <xsl:value-of select="root/label[@id='jsPrevLinkText']/@text"/>',
             nextLinkText: '<xsl:value-of select="root/label[@id='jsNextLinkText']/@text"/> &#8250;',
             nextPageLinkText: '<xsl:value-of select="root/label[@id='jsNextPageLinkText']/@text"/> &#8250;',
             prevPageLinkText: '&#8249; <xsl:value-of select="root/label[@id='jsPrevPageLinkText']/@text"/>',
        enableHistory: true,
        autoStart: false,
        syncTransitions: true,
        defaultTransitionDuration: 900,
        onSlideChange:             function(prevIndex, nextIndex) {
        // 'this' refers to the gallery, which is an extension of $('#thumbs')
        this.find('ul.thumbs').children()
        .eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
        .eq(nextIndex).fadeTo('fast', 1.0);
        },
        onPageTransitionOut:       function(callback) {
        this.fadeTo('fast', 0.0, callback);
        },
        onPageTransitionIn:        function() {
        this.fadeTo('fast', 1.0);
        }

        });

        /**** Functions to support integration of galleriffic with the jquery.history plugin ****/

        // PageLoad function
        // This function is called when:
        // 1. after calling $.historyInit();
        // 2. after calling $.historyLoad();
        // 3. after pushing "Go Back" button of a browser
        function pageload(hash) {
        // alert("pageload: " + hash);
        // hash doesn't contain the first # character.
        if(hash) {
        $.galleriffic.gotoImage(hash);
        } else {
        gallery.gotoIndex(0);
        }
        }

        // Initialize history plugin.
        // The callback is called at once by present location.hash.
        $.historyInit(pageload, "advanced.html");

        // set onlick event for buttons using the jQuery 1.3 live method
        $("a[rel='history']").live('click', function(e) {
        if (e.button != 0) return true;

        var hash = this.href;
        hash = hash.replace(/^.*#/, '');

        // moves to a new page.
        // pageload is called at once.
        // hash don't contain "#", "?"
        $.historyLoad(hash);

        return false;
        });

        /****************************************************************************************/
        });
      </script>
    </xsl:template>
</xsl:stylesheet>
