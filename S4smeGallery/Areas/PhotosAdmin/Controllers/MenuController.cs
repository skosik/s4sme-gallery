﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace S4smeGallery.Areas.PhotosAdmin.Controllers
{
    public class MenuController : Controller
    {
        //
        // GET: /PhotosAdmin/Menu/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /PhotosAdmin/Menu/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /PhotosAdmin/Menu/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /PhotosAdmin/Menu/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /PhotosAdmin/Menu/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /PhotosAdmin/Menu/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /PhotosAdmin/Menu/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /PhotosAdmin/Menu/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
