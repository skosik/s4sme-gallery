﻿using System.Web.Mvc;

namespace S4smeGallery.Areas.PhotosAdmin
{
    public class PhotosAdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PhotosAdmin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PhotosAdmin_default",
                "PhotosAdmin/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
