﻿using System.Threading;
using System.Web.Mvc;

namespace S4smeGallery.Infrastructure
{
  public abstract class LocalisedControllerBase : Controller
  {
    public string LanguageCode { get; private set; }

    protected override void Execute(System.Web.Routing.RequestContext requestContext)
    {
      // Get the culture code from Url
        var culture = requestContext.RouteData.Values["culture"];
        var currentCulture = culture != null ? culture.ToString() : "en-IE";

        // Set the current culture thread
      Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.GetCultureInfoByIetfLanguageTag(currentCulture);
      Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.GetCultureInfoByIetfLanguageTag(currentCulture);

      base.Execute(requestContext);
    }
  }
}
